"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
import csv
import os
import json
import io
from flask import redirect, url_for, request, render_template, make_response
from flask_restful import Resource, Api, reqparse
from flask_cors import CORS
from pymongo import MongoClient
# import arrow  # Replacement for datetime, based on moment.js
import acp_times
# import config
import logging

########################
#  Globals
########################

# CONFIG = config.configuration()
app = flask.Flask(__name__)
app.config['JSONIFY_PRETTYPRINT_REGULAR'] = False
cors = CORS(app, origins='*')
app.config['CORS_HEADERS'] = 'Content-Type'
app.secret_key = b'f\xab\x9d\xf1\xc0\x85\xccU\xde\x97f\x05uA\xc3\xd6\xaa\x89\xe0`\n_)\xa8'

api = Api(app)

MONGODB_HOST = "db"
client = MongoClient(MONGODB_HOST, 27017)

db = client.brevet_calc

#################################
#  Routes
#################################

@app.route('/todo')
def todo():
    _items = db.brevet_calc.find()
    items = [item for item in _items]
    # app.logger.info(items)
    # app.logger.info(len(items))
    return render_template('todo.html', items=items)


# https://www.diffen.com/difference/GET-vs-POST-HTTP-Requests
@app.route('/new', methods=['POST'])
def new():
    data_list = []
    miles_array = request.form.getlist('miles[]')
    km_array = request.form.getlist('km[]')
    loc_array = request.form.getlist('location[]')
    open_array = request.form.getlist('open[]')
    close_array = request.form.getlist('close[]')
    for i in range(len(miles_array)):
        if len(miles_array[i]) != 0:
            data_list.append({"miles": miles_array[i],
                              "km": km_array[i],
                              "location": loc_array[i],
                              "open": open_array[i],
                              "close": close_array[i]
                              })
    item_doc = {
        'distance': request.form['distance'],
        'begin_date': request.form['begin_date'],
        'begin_time': request.form['begin_time'],
        'control_data': data_list
    }
    db.brevet_calc.insert_one(item_doc)
    return redirect(url_for('index'))


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.info("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers - These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    control_dist_km = request.args.get('km', 999, type=float)  # control kms
    brevet_dist = request.args.get('brevet_dist', type=float)
    begin_date = request.args.get('begin_date')
    begin_time = request.args.get('begin_time')
    # app.logger.info(f"km={control_dist_km}, brevet_dist={brevet_dist}, date={begin_date}, time={begin_time}")
    # app.logger.info(f"request.args: {request.args}")
    race_start = f"{begin_date} {begin_time}"
    try:
        open_time = acp_times.open_time(control_dist_km, brevet_dist, race_start)
    except Exception as e:
        app.logger.exception(e)
        # error_msg = "Control point is too far beyond brevet distance!\nPlease try a shorter distance."
        error_msg = str(e)
        result = {"error_msg": error_msg}
    else:
        close_time = acp_times.close_time(control_dist_km, brevet_dist, race_start)
        is_final = acp_times.final_control(brevet_dist, control_dist_km)
        result = {"open": open_time, "close": close_time, "final": is_final}
    return flask.jsonify(result=result)


#####################
#  REST-ful APIs
#####################

class AllBrevets(Resource):
    # parser = reqparse.RequestParser()
    # parser.add_argument('top', type=int)

    def get(self):
        app.logger.info("HEEEEEEEEEEERE!!!!")
        # args = AllBrevets.parser.parse_args()
        # app.logger.info(args['top'])
        brevets = []
        _items = db.brevet_calc.find()
        for item in _items:
            brevet = dict()
            brevet["distance"] = item["distance"]
            brevet["begin_date"] = item["begin_date"]
            brevet["begin_time"] = item["begin_time"]
            brevet["control_data"] = []
            for control in item["control_data"]:
                new_control = dict()
                new_control["km"] = control['km']
                new_control["miles"] = control["miles"]
                new_control["location"] = control["location"]
                new_control["open"] = control["open"]
                new_control["close"] = control["close"]
                brevet["control_data"].append(new_control)
            brevets.append(brevet)
        response_obj = {"brevets": brevets}
        return response_obj


class AllOpen(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('top', type=int)

    def get(self):
        args = AllOpen.parser.parse_args()
        app.logger.info(args['top'])
        top = args['top']
        brevets = []
        _items = db.brevet_calc.find()
        if top:
            _items = sorted(_items, key=lambda x: x["control_data"][0]['open'], reverse=False)[0:top]
        for item in _items:
            brevet = dict()
            brevet["distance"] = item["distance"]
            brevet["begin_date"] = item["begin_date"]
            brevet["begin_time"] = item["begin_time"]
            brevet["control_data"] = []
            index = 0
            for control in item["control_data"]:
                if top and index > 0:
                    break
                new_control = dict()
                new_control["km"] = control['km']
                new_control["miles"] = control["miles"]
                new_control["location"] = control["location"]
                new_control["open"] = control["open"]
                brevet["control_data"].append(new_control)
                index += 1
            brevets.append(brevet)
        response_obj = {"brevets": brevets}
        return response_obj


class AllClose(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('top', type=int)

    def get(self):
        args = AllClose.parser.parse_args()
        app.logger.info(args['top'])
        top = args['top']
        brevets = []
        _items = db.brevet_calc.find()
        if top:
            _items = sorted(_items, key=lambda x: x["control_data"][0]['open'], reverse=False)[0:top]
        for item in _items:
            brevet = dict()
            brevet["distance"] = item["distance"]
            brevet["begin_date"] = item["begin_date"]
            brevet["begin_time"] = item["begin_time"]
            brevet["control_data"] = []
            index = 0
            for control in item["control_data"]:
                if top and index > 0:
                    break
                new_control = dict()
                new_control["km"] = control['km']
                new_control["miles"] = control["miles"]
                new_control["location"] = control["location"]
                new_control["close"] = control["close"]
                brevet["control_data"].append(new_control)
                index += 1
            brevets.append(brevet)
        response_obj = {"brevets": brevets}
        return response_obj


class AllBrevetsCSV(Resource):
    """CSV-downloadable file attempt."""

    def get(self):
        rows = []
        _items = db.brevet_calc.find()
        last_max = 0
        for item in _items:
            distance = item['distance']
            begin_date = item['begin_date']
            begin_time = item['begin_time']
            row_data = [distance, begin_date, begin_time]
            if len(item['control_data']) > last_max:
                last_max = len(item['control_data'])
            for control in item['control_data']:
                km = control['km']
                row_data.append(km)
                miles = control['miles']
                row_data.append(miles)
                location = control['location']
                row_data.append(location)
                opens = control['open']
                row_data.append(opens)
                close = control['close']
                row_data.append(close)
            rows.append(row_data)
        header_row = ["brevets/distance", "brevets/begin_date", "brevets/begin_time"]
        for i in range(last_max):
            header_row.append(f"brevets/controls/{i}/km")
            header_row.append(f"brevets/controls/{i}/miles")
            header_row.append(f"brevets/controls/{i}/location")
            header_row.append(f"brevets/controls/{i}/open")
            header_row.append(f"brevets/controls/{i}/close")
        rows.insert(0, header_row)
        si = io.StringIO()
        cw = csv.writer(si)
        cw.writerows(rows)
        output = make_response(si.getvalue())
        # output.headers['Content-Disposition'] = 'inline; filename=test.csv'
        # output.headers["Content-Disposition"] = "attachment; filename=export.csv"
        # output.headers["Content-type"] = "text/csv"
        return output


# class AllBrevetsCSV(Resource):
#     """Stringified attempt."""
#     def get(self):
#         rows = ""
#         _items = db.brevet_calc.find()
#         last_max = 0
#         row_data = ""
#         for item in _items:
#             if len(item['control_data']) > last_max:
#                 last_max = len(item['control_data'])
#             for control in item['control_data']:
#                 km = control['km']
#                 row_data += f",{km}"
#                 miles = control['miles']
#                 row_data += f",{miles}"
#                 location = control['location']
#                 row_data += f",{location}"
#                 opens = control['open']
#                 row_data += f",{opens}"
#                 close = control['close']
#                 row_data += f",{close}"
#             row_data += "\n"
#         # header_row = ["brevets/distance", "brevets/begin_date", "brevets/begin_time"]
#         header_row = "brevets/distance,brevets/begin_date,brevets/begin_time"
#         for i in range(last_max):
#             header_row += f",brevets/controls/{i}/km"
#             header_row += f",brevets/controls/{i}/miles"
#             header_row += f",brevets/controls/{i}/location"
#             header_row += f",brevets/controls/{i}/open"
#             header_row += f",brevets/controls/{i}/close"
#         # ",".join(header_row)
#         # rows.insert(0, header_row)
#         # si = io.StringIO()
#         # cw = csv.writer(si)
#         # cw.writerows(rows)
#         # output = make_response(si.getvalue())
#         header_row = header_row + row_data
#         output = make_response(header_row)
#         # output.headers["Content-Disposition"] = "attachment; filename=export.csv"
#         output.headers["Content-type"] = "text/csv"
#         return header_row


class AllBrevetsOpenCSV(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('top', type=int)

    def get(self):
        args = AllBrevetsOpenCSV.parser.parse_args()
        top = args['top']
        rows = []
        _items = db.brevet_calc.find()
        if top:
            _items = sorted(_items, key=lambda x: x["control_data"][0]['open'], reverse=False)[0:top]
        last_max = 0
        for item in _items:
            distance = item['distance']
            begin_date = item['begin_date']
            begin_time = item['begin_time']
            row_data = [distance, begin_date, begin_time]
            if len(item['control_data']) > last_max:
                last_max = len(item['control_data'])
            index = 0
            for control in item["control_data"]:
                if top and index > 0:
                    break
                km = control['km']
                row_data.append(km)
                miles = control['miles']
                row_data.append(miles)
                location = control['location']
                row_data.append(location)
                opens = control['open']
                row_data.append(opens)
                index += 1
            rows.append(row_data)
        header_row = ["brevets/distance", "brevets/begin_date", "brevets/begin_time"]
        for i in range(last_max):
            if top and i > 0:
                break
            header_row.append(f"brevets/controls/{i}/km")
            header_row.append(f"brevets/controls/{i}/miles")
            header_row.append(f"brevets/controls/{i}/location")
            header_row.append(f"brevets/controls/{i}/open")
        rows.insert(0, header_row)
        si = io.StringIO()
        cw = csv.writer(si)
        cw.writerows(rows)
        output = make_response(si.getvalue())
        # output.headers["Content-Disposition"] = "attachment; filename=export.csv"
        # output.headers["Content-type"] = "text/csv"
        return output


class AllBrevetsCloseCSV(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('top', type=int)

    def get(self):
        args = AllBrevetsCloseCSV.parser.parse_args()
        top = args['top']
        rows = []
        _items = db.brevet_calc.find()
        if top:
            _items = sorted(_items, key=lambda x: x["control_data"][0]['open'], reverse=False)[0:top]
        last_max = 0
        for item in _items:
            distance = item['distance']
            begin_date = item['begin_date']
            begin_time = item['begin_time']
            row_data = [distance, begin_date, begin_time]
            if len(item['control_data']) > last_max:
                last_max = len(item['control_data'])
            index = 0
            for control in item["control_data"]:
                if top and index > 0:
                    break
                km = control['km']
                row_data.append(km)
                miles = control['miles']
                row_data.append(miles)
                location = control['location']
                row_data.append(location)
                close = control['close']
                row_data.append(close)
                index += 1
            rows.append(row_data)
        header_row = ["brevets/distance", "brevets/begin_date", "brevets/begin_time"]
        for i in range(last_max):
            if top and i > 0:
                break
            header_row.append(f"brevets/controls/{i}/km")
            header_row.append(f"brevets/controls/{i}/miles")
            header_row.append(f"brevets/controls/{i}/location")
            header_row.append(f"brevets/controls/{i}/close")
        rows.insert(0, header_row)
        si = io.StringIO()
        cw = csv.writer(si)
        cw.writerows(rows)
        output = make_response(si.getvalue())
        # output.headers["Content-Disposition"] = "attachment; filename=export.csv"
        # output.headers["Content-type"] = "text/csv"
        return output


# class AllBrevetsCSV(Resource):
    # def get(self):
    #     brevets = []
    #     _items = db.brevet_calc.find()
    #     for item in _items:
    #         brevet = dict()
    #         brevet["distance"] = item["distance"]
    #         brevet["begin_date"] = item["begin_date"]
    #         brevet["begin_time"] = item["begin_time"]
    #         brevet["control_data"] = []
    #         for control in item["control_data"]:
    #             new_control = dict()
    #             new_control["km"] = control['km']
    #             new_control["miles"] = control["miles"]
    #             new_control["location"] = control["location"]
    #             new_control["open"] = control["open"]
    #             new_control["close"] = control["close"]
    #             brevet["control_data"].append(new_control)
    #         brevets.append(brevet)
    #     response_obj = {"brevets": brevets}
    #     return response_obj


    # @app.route('/download')
    # def post(self):
    #     si = StringIO.StringIO ()
    #     cw = csv.writer (si)
    #     cw.writerows (csvList)
    #     output = make_response (si.getvalue ())
    #     output.headers["Content-Disposition"] = "attachment; filename=export.csv"
    #     output.headers["Content-type"] = "text/csv"
    #     return output

# class AllBrevetsCSV(Resource):
#     def get(self):
#         with open('test_file.csv', 'w', newline='') as csvfile:
#             testwriter = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_MINIMAL)
#             _items = db.brevet_calc.find()
#             for item in _items:
#                 distance = item['distance']
#                 begin_date = item['begin_date']
#                 begin_time = item['begin_time']
#                 row_data = [distance, begin_date, begin_time]
#                 testwriter.writerow(row_data)
#         return csvfile

# @api.representation('text/csv')
# def output_csv(data, code, headers=None):
#     """Makes a Flask response with a .csv encoded body."""
#     si = io.StringIO()
#     cw = csv.writer(si)
#     cw.writerows(data)
#     output = make_response(si.getvalue())
#     # output.headers["Content-Disposition"] = "attachment; filename=export.csv"
#     output.headers["Content-type"] = "text/csv"
#     # resp = make_response(data, code)
#     # resp.headers.extend(headers or {})
#     # return resp
#     return output


# def output_json(data, code, headers=None):
#     """Makes a Flask response with a JSON encoded body"""
#     resp = make_response(json.dumps(data), code)
#     resp.headers.extend(headers or {})
#     return resp


api.add_resource(AllBrevets, '/listAll', '/listAll/json')
api.add_resource(AllOpen, '/listOpenOnly', '/listOpenOnly/json')
api.add_resource(AllClose, '/listCloseOnly', '/listCloseOnly/json')


api.add_resource(AllBrevetsCSV, '/listAll/csv')
api.add_resource(AllBrevetsOpenCSV, '/listOpenOnly/csv')
api.add_resource(AllBrevetsCloseCSV, '/listCloseOnly/csv')

# api.add_resource(AllBrevetsCSV, '/listAll/csv', resource_class_kwargs={'representations': {'text/csv': output_csv}})


# app.debug = CONFIG.DEBUG
# if app.debug:
#     app.logger.setLevel(logging.DEBUG)

# if __name__ == "__main__":
#     # print(f"Opening for global access on port {CONFIG.PORT}")
#     app.run(port=CONFIG.PORT, host="0.0.0.0", debug=True)

# Run the application

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
