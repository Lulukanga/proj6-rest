<html>
    <head>
        <title>CIS 322 Brevets REST-api</title>
            <!-- Javascript:  JQuery from a content distribution network (CDN) -->
            <script
             src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js">
            </script>
    </head>

    <body>
        <h1>API Options</h1>
        <br><br>
        <p>What kind of brevet information would you like? </p>
        <form onsubmit="OnSubmit(event)">

        <select name="info_options" id="info">
            <option value="listAll"selected>All Opening and Closing times</option>
            <option value="listCloseOnly">Closing Times Only</option>
            <option value="listOpenOnly">Opening Times Only</option>
        </select>

        <br><br>
        <p>Select formatting options</p>
        <select name="format_options" id="formatting">
            <option value="csv" selected>CSV</option>
            <option value="json">JSON</option>
        </select>
        <br><br><br><br>

        <div>
            <label>How many items would you like to see? </label>
            <input type=number name="top" id="k_items" min="1"/>
        </div>

        <div>
        <button type="submit" style="height:50px;width:100px;margin-top: 60px;">Submit</button>
        <button type="button" id="clear" onclick="clearData()" style="height:50px;width:100px;margin-left: 60px;">Clear Data</button>
        </div>
        </form>

         <div style="width:80%;overflow:auto;padding-bottom:60px">
            <pre id="results"></pre>
         </div>

    <script type="text/javascript">
             function OnSubmit(event) {
                event.preventDefault();
                var info_option = $("#info").val();
                var k = $("#k_items").val();
                var format = $("#formatting").val();
                var query_string = info_option + "/" + format
                if (k.length >= 1) {
                    query_string += "?top=" + k
                }

                $.get('http://localhost:5001/'+query_string, function(data) {
                    console.log(data)
                    if (format === 'json') {
                        $("#results").html(JSON.stringify(data, null, 2))
                    } else {
                        $("#results").html(data)
                    }

                }
                )
             }

             function clearData(event) {
                console.log("you wanna clear some data? ")
                $("#results").html("")
             }
    </script>

    </body>
</html>
