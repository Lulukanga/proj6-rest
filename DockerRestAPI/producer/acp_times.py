# """
# Open and close time calculations
# for ACP-sanctioned brevets
# following rules described at https://rusa.org/octime_alg.html
# and https://rusa.org/pages/rulesForRiders
# """
#
# import arrow
#
# # Minimum times as [(from_dist, to_dist, speed),
# #                   (from_dist, to_dist, speed), ... ]
# min_speed = [(0, 200, 15), (200, 400, 15), (400, 600, 15),
#              (600, 1000, 11.428), (1000, 1300, 13.333)]
#
# max_speed = [(0, 200, 34), (200, 400, 32), (400, 600, 30),
#              (600, 1000, 28), (1000, 1300, 28)]
#
# # Final control times (at or exceeding brevet distance) are special cases
# final_close = {200: 13.5, 300: 20, 400: 27, 600: 40, 1000: 75}
# max_dist = 1300
#
#
# def final_control(brevet_dist_km, control_dist_km):
#     return (brevet_dist_km - control_dist_km) <= 0
#
#
# def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
#     """
#     Args:
#        control_dist_km:  number, the control distance in kilometers
#        brevet_dist_km: number, the nominal distance of the brevet
#        in kilometers, which must be one of 200, 300, 400, 600, or
#            1000 (the only official ACP brevet distances)
#        brevet_start_time:  An ISO 8601 format date-time string
#            indicating the official start time of the brevet
#     Returns:
#        An ISO 8601 format date string indicating the control open time.
#        This will be in the same time zone as the brevet start time.
#     """
#     start_time = arrow.get(brevet_start_time)
#     elapsed_hours = 0
#     distance_left = control_dist_km
#     for from_dist, to_dist, speed in max_speed:
#         seg_length = to_dist - from_dist
#         if distance_left > seg_length:
#             elapsed_hours += seg_length / speed
#             distance_left -= seg_length
#         else:
#             elapsed_hours += distance_left / speed
#             open_time = start_time.replace(hours=elapsed_hours)
#             return open_time.isoformat()
#
#
# def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
#     """
#     Args:
#        control_dist_km:  number, the control distance in kilometers
#        brevet_dist_km: number, the nominal distance of the brevet
#            in kilometers, which must be one of 200, 300, 400, 600,
#            or 1000 (the only official ACP brevet distances)
#        brevet_start_time:  An ISO 8601 format date-time string indicating
#            the official start time of the brevet
#     Returns:
#        An ISO 8601 format date string indicating the control close time.
#        This will be in the same time zone as the brevet start time.
#     """
#     start_time = arrow.get(brevet_start_time)
#     if control_dist_km >= brevet_dist_km:
#         duration = final_close[brevet_dist_km]
#         finish_time = start_time.replace(hours=duration)
#         return finish_time.isoformat()
#     elapsed_hours = 0
#     distance_left = control_dist_km
#     for from_dist, to_dist, speed in min_speed:
#         seg_length = to_dist - from_dist
#         if distance_left > seg_length:
#             elapsed_hours += seg_length / speed
#             distance_left -= seg_length
#         else:
#             elapsed_hours += distance_left / speed
#             cut_time = start_time.replace(hours=elapsed_hours)
#             return cut_time.isoformat()
#     return arrow.now().isoformat()


"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow
import math

#  Note for CIS 322:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.


def final_control(brevet_dist_km, control_dist_km):
    return (brevet_dist_km - control_dist_km) <= 0


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    if control_dist_km < 0:
        raise Exception("Invalid Control Distance\nOnly positive distances accepted.")
    CONTROL_OPENS = {200: [math.floor(control_dist_km/34),
                           round(((control_dist_km/34) % 1) * 60)],

                     300: [math.floor((200/34) + (control_dist_km - 200) / 32),
                           round((((200 / 34) + ((control_dist_km - 200) / 32)) % 1) * 60)],

                     400: [math.floor((200/34) + (control_dist_km - 200) / 32),
                           round((((200/34) + ((control_dist_km - 200)/32)) % 1) * 60)],

                     600: [math.floor((200/34) + (200/32) + ((control_dist_km - 400) / 30)),
                           round((((200/34) + (200/32) + ((control_dist_km - 400) / 30)) % 1) * 60)],

                     1000: [math.floor((200/34) + (200/32) + (200/30) + ((control_dist_km - 600) / 28)),
                            round((((200/34) + (200/32) + (200/30) + ((control_dist_km - 600) / 28)) % 1) * 60)]}

    FINAL_OPENS = {200: [5, 53],
                   300: [9, 00],
                   400: [12, 8],
                   600: [18, 48],
                   1000: [33, 5]}
    time = arrow.get(brevet_start_time, 'YYYY-MM-DD HH:mm')
    final = final_control(brevet_dist_km, control_dist_km)
    WIGGLE_ROOM = brevet_dist_km * .2  # Should be 20% of total brevet distance # TODO: verify this.
    for key in CONTROL_OPENS.keys():
        if control_dist_km < key:
            hour_shift = CONTROL_OPENS[key][0]
            minute_shift = CONTROL_OPENS[key][1]
            break
        elif final and (control_dist_km <= (key + WIGGLE_ROOM)):
            hour_shift = FINAL_OPENS[key][0]
            minute_shift = FINAL_OPENS[key][1]
            break
        elif final and control_dist_km > (brevet_dist_km + WIGGLE_ROOM):
            raise Exception("Control point is too far beyond brevet distance!\nPlease try a shorter distance.")
    return time.shift(hours=+hour_shift, minutes=+minute_shift).isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    # return arrow.now().isoformat()
    CONTROL_CLOSES = {0: [1, 0],  # Starting points always close one hour after open
                      200: [math.floor(control_dist_km/15), ((control_dist_km/15) % 1) * 60],
                      300: [math.floor(control_dist_km/15), ((control_dist_km/15) % 1) * 60],
                      400: [math.floor(control_dist_km/15), ((control_dist_km/15) % 1) * 60],
                      600: [math.floor(control_dist_km/15), ((control_dist_km/15) % 1) * 60],
                      1000: [math.floor((600/15) + ((control_dist_km - 600)/11.428)),
                             (((600/15) + ((control_dist_km - 600)/11.428)) % 1) * 60]}
    CONTROL_FINAL = {200: [13, 30],
                     300: [20, 0],
                     400: [27, 0],
                     600: [40, 0],
                     1000: [75, 0]}
    time = arrow.get(brevet_start_time, 'YYYY-MM-DD HH:mm')
    final = final_control(brevet_dist_km, control_dist_km)
    hour_shift = 0
    minute_shift = 0
    for key in CONTROL_CLOSES.keys():
        if control_dist_km <= key:
            if control_dist_km <= 60:
                hours = (control_dist_km/20) + 1
                hour_shift = math.floor(hours)
                minute_shift = (hours % 1) * 60
            else:
                hour_shift = CONTROL_CLOSES[key][0]
                minute_shift = CONTROL_CLOSES[key][1]
            break
    if final:
        for key in CONTROL_FINAL.keys():
            if control_dist_km >= key:
                hour_shift = CONTROL_FINAL[key][0]
                minute_shift = CONTROL_FINAL[key][1]
    return time.shift(hours=+hour_shift, minutes=+minute_shift).isoformat()
