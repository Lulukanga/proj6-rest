# Project 6: Brevet time calculator service

**Author**: Lindsey Uribe

**Contact**: luribe@uoregon.edu

## Basic Functionality

This project has following four parts: 

**Producer Site: Create Brevets and add Brevet data to the database at http://localhost:5001**

  * All of the prior functionality of the original producer site remains: 
    
    * Select brevet distance
    * Select a start-date and time
    * Enter in preferred brevet controle data. 
  * Upon completion, submit entries to the data base by using the 'Submit' button
  * Once data has been saved to the database, that data is viewable via the 'Display' button.

**Consumer Site: View Brevets and Brevet data via RESTful service exposure at http://localhost:5000**

  * All of the services exposed by the API are available at this site in convenient, format-specific views. 
    
  * Select from the following options:
    
      * Dropdown menu: 
        * **All Opening and Closing times** : returns all opening and closing times in the database via a GET request to `http://localhost:5000/listAll` 
        * **Opening Times Only** : returns all controle open times for each brevet in the data base via a GET request to `http://localhost:5000/listOpenOnly` 
        * **Closing Times Only** : returns all controle close times for each brevet in the data base via a GET request to `http://localhost:5000/listCloseOnly`
        
      * Select formatting options: 
        
        * **CSV**: If selected, will return either...
          * All controle open and close times, in CSV formatting, via a GET request to `http://localhost:5000/listAll/csv` 
          * All controle open times, in CSV formatting, via a GET request to `http://localhost:5000/listOpenOnly/csv`
          * All controle close times, in CSV formatting, via a GET request to `http://localhost:5000/listCloseOnly/csv`
          
        * **JSON**: If selected, will return either...
          * All controle open and close times, in JSON formatting, via a GET request to `http://localhost:5000/listAll/json` 
          * All controle open times, in JSON formatting, via a GET request to `http://localhost:5000/listOpenOnly/json`
          * All controle close times, in JSON formatting, via a GET request to `http://localhost:5000/listCloseOnly/json` 
      
      * How many items would you like to view?
        
        * Selects the top `k` items to display for either formatting option (CSV or JSON) for open times or close times.
        * Note: Selecting any number of top `k` items with **All Opening and Closing times** selected from the drop down menu above has no effect on the output data. All open and closing times will be displayed.
          * Selecting `3` while specifying **Opening Times Only** and **CSV** formatting will retrieve the top 3 open times only (in ascending order) in CSV format, sorted by controle open times via a GET request to `http://localhost:5000/listOpenOnly/csv?top=3`
          * Selecting `5` while specifying **Opening Times Only** and **JSON** formatting will retrieve the top 5 open times only (in ascending order) in JSON format, sorted by controle open times via a GET request to `http://localhost:5000/listOpenOnly/json?top=5`
          * Selecting `6` while specifying **Closing Times Only** and **CSV** formatting will retrieve the top 6 close times only (in ascending order) in CSV format, sorted by controle open times via a GET request to `http://localhost:5000/listCloseOnly/csv?top=6` will retrieve the top 5 close times only (in ascending order) in CSV format, sorted by controle close times.
          * Selecting `4` while specifying **Closing Times Only** and **JSON** formatting will retrieve the top 4 close times only (in ascending order) in JSON format, sorted by controle open times via a GET request to `http://localhost:5000/listCloseOnly/json?top=4` will retrieve the top 4 close times only (in ascending order) in JSON format, sorted by controle close times.

* Data URIs: All data from the database is also available via navigable, bookmarkable links at the following addresses
  
    * `http://localhost:5000/listAll/csv` displays all open and close times in CSV format
    * `http://localhost:5000/listOpenOnly/csv` displays all open times only in CSV format
    * `http://localhost:5000/listCloseOnly/csv` displays all close times only in CSV format

    * `http://localhost:5000/listAll/json` displays all open and close times in JSON format
    * `http://localhost:5000/listOpenOnly/json` displays all open times only in JSON format
    * `http://localhost:5000/listCloseOnly/json` displays all close times only in JSON format
  
    * `http://localhost:5000/listOpenOnly/csv?top=3` displays the top 3 open times only, in CSV format
    * `http://localhost:5000/listCloseOnly/csv?top=5` displays the top 5 close times only, in CSV format
    * `http://localhost:5000/listOpenOnly/json?top=2` displays the top 2 open times only, in JSON format
    * `http://localhost:5000/listCloseOnly/json?top=4` displays the top 4 close times only, in JSON format
